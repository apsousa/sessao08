package com.example.android.meucontatos;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetalhesActivity extends AppCompatActivity {

    private static final int MEU_CODIGO_DE_PERMISSAO_PARA_LIGAR = 88;

    private ImageView ivFoto;
    private TextView tvNome;
    private ImageButton ibTelefone;
    private ImageButton ibSms;
    private String telefone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        ivFoto = (ImageView) findViewById(R.id.iv_foto);
        tvNome = (TextView) findViewById(R.id.tv_nome);
        ibTelefone = (ImageButton) findViewById(R.id.ib_telefone);
        ibSms = (ImageButton) findViewById(R.id.ib_sms);

        // Obter o intent que iniciou esta atividade e extrair o extra do tipo bundle
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("contato");

        ivFoto.setImageResource(bundle.getInt("idimagem"));
        tvNome.setText(bundle.getString("nome"));
        telefone = bundle.getString("telefone");
    }

    public void executa(View view) {
        if( view.getId() == R.id.ib_sms) {
            Uri uri = Uri.parse("smsto:" + telefone);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            startActivity(intent);
        } else {
            preparaPermissoes();
        }
    }

    private void preparaPermissoes() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                String[] permissionsWeNeed = new String[]{ Manifest.permission.CALL_PHONE };
                requestPermissions(permissionsWeNeed, MEU_CODIGO_DE_PERMISSAO_PARA_LIGAR);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + telefone));
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MEU_CODIGO_DE_PERMISSAO_PARA_LIGAR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permissão concedida
                } else {
                    Toast.makeText(this, "Não autorizou a ligação.", Toast.LENGTH_LONG).show();
                    finish();
                }
            }


        }
    }
}
