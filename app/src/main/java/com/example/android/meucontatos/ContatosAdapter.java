package com.example.android.meucontatos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by asousa on 20-02-2018.
 */

public class ContatosAdapter extends  RecyclerView.Adapter<ContatosAdapter.ContatoHolder> {

    private ArrayList<Contato> mContatos;
    private Context mContext;
    private Toast mToast;

    public ContatosAdapter(ArrayList<Contato> contatos, Context context) {
        mContatos = contatos;
        mContext = context;
    }

    @Override
    public ContatoHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);

        ContatoHolder vh = new ContatoHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ContatoHolder holder, int position) {
        //holder.nomeReceita.setText(mContatos.get(position));
        holder.nomeContato.setText(mContatos.get(position).getNome().toString());
        holder.fotoContato.setImageResource(mContatos.get(position).getIdImage());

    }

    @Override
    public int getItemCount() {
        return mContatos.size();
    }


    /*
    --------- VIEW HOLDER -----------
     */
    public class ContatoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView nomeContato;
        public ImageView fotoContato;

        public ContatoHolder(View itemView) {
            super(itemView);
            nomeContato = itemView.findViewById(R.id.tv_nome_contato);
            fotoContato = itemView.findViewById(R.id.iv_foto_thumbnail);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if( mToast != null ) {
                mToast.cancel();
            }
            mToast = Toast.makeText(mContext,"Contato clicado: ", Toast.LENGTH_SHORT);
            mToast.show();

            // Abrir a atividade com os detalhes do contato
            Intent intent = new Intent(mContext, DetalhesActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("nome", mContatos.get(getAdapterPosition()).getNome());
            bundle.putString("telefone", mContatos.get(getAdapterPosition()).getTelefone());
            bundle.putInt("idimagem", mContatos.get(getAdapterPosition()).getIdImage());
            intent.putExtra("contato", bundle);

            mContext.startActivity(intent);
        }
    }

    /*
    ----------------------------------
     */


}
