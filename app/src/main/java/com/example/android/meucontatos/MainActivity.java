package com.example.android.meucontatos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Contato> contatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contatos = new ArrayList<>(Arrays.asList(
                new Contato("Angelina Jolly", "960000001", R.drawable.angelina_jolly),
                new Contato("Anthony Hopkins", "960000002", R.drawable.anthony_hopkins),
                new Contato("Antonio Banderas", "960000003", R.drawable.antonio_banderas),
                new Contato("Avril Levigne", "960000004", R.drawable.avril_levigne),
                new Contato("Ben Affleck", "960000005", R.drawable.ben_affleck),
                new Contato("Britney Spears", "960000006", R.drawable.britney_spears),
                new Contato("Daniel Craig", "960000007", R.drawable.daniel_craig),
                new Contato("John Lennon", "960000008", R.drawable.john_lennon),
                new Contato("Johnny Depp", "960000009", R.drawable.johnny_depp),
                new Contato("Maria Carey", "960000010", R.drawable.maria_carey),
                new Contato("Mel Gibson", "960000011", R.drawable.mel_gibson),
                new Contato("Mick Jagger", "960000012", R.drawable.mick_jagger)
        ));

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_contatos);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ContatosAdapter(contatos, this);
        mRecyclerView.setAdapter(mAdapter);


    }
}
