package com.example.android.meucontatos;

/**
 * Created by asousa on 20-02-2018.
 */

public class Contato {

    private String mNome;
    private String mTelefone;
    private int mIdImage;

    public Contato(String nome, String telefone, int idImage) {
        mNome = nome;
        mTelefone = telefone;
        mIdImage = idImage;
    }

    public String getNome() {
        return mNome;
    }

    public void setNome(String nome) {
        mNome = nome;
    }

    public String getTelefone() {
        return mTelefone;
    }

    public void setTelefone(String telefone) {
        mTelefone = telefone;
    }

    public int getIdImage() {
        return mIdImage;
    }

    public void setIdImage(int idImage) {
        mIdImage = idImage;
    }
}
